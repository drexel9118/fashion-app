import * as React from 'react';
import { StyleSheet, View, Dimensions, Text } from 'react-native';

interface slideProps {
    label: string;
     right?: boolean;
}

const {width, height} = Dimensions.get('window')

const SLIDE_HEIGHT = 0.61 * height

const Slide: React.SFC<slideProps> = ({label, right}: slideProps) => {

    const transform = [
        {translateY: (SLIDE_HEIGHT - 100)/2},
        {translateX: right? width / 2 - 50: -width/2 + 50},
        {rotate: right ? '-90deg': '90deg'}
    ]

  return (
    <View style={styles.container}>
        <View style={[styles.titleContainer, {transform}]}>
            <Text style={styles.title}>{label}</Text>
        </View>
    </View>
  ); 
}

const styles = StyleSheet.create({
    container: {
        width,
    },
    title: {
        fontSize: 80,
        color: 'white',
        textAlign: 'center',
        lineHeight: 80,
        fontWeight: 'bold'
    },
    titleContainer: {
        height: 100,
        justifyContent: 'center',
    }
})

export default Slide;