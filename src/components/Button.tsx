import * as React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { RectButton } from 'react-native-gesture-handler';

interface ButtonProps {
    variant: 'default' | 'primary';
    label: string;
    onPress: ()=> void;
}

const Button: React.SFC<ButtonProps> = ({variant, label, onPress}: ButtonProps) => {
  const backgroundColor = variant === 'primary' ? '#2CB9B0' : 'rgba(12, 13, 52, 0.05)'
  const color = variant === 'primary' ? 'white' : '#0C0D34'
  return (
    <RectButton onPress={onPress} style={[styles.container, {backgroundColor}]}>
        <Text style={[styles.label, {color}]}>{label}</Text>
    </RectButton>
  ); 
}

const styles = StyleSheet.create({
    container: {
        borderRadius: 25,
        height: 50,
        width: 245,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        fontSize: 18,
        textAlign: 'center',
        fontWeight: 'bold',
    }
})

Button.defaultProps = {variant: 'default'}

export default Button;