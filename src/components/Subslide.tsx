import * as React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import Button from './Button';

interface subSlideProps {
    title: string;
    description: string;
    isLast?: boolean;
    onPress: ()=> void;
}

const SubSlide: React.SFC<subSlideProps> = ({title, description, isLast, onPress}: subSlideProps) => {

  return (
    <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>{description}</Text>
        <Button 
            label={isLast? `Let's get started` : 'Next'}
            variant={isLast? 'primary': 'default'}
            {...{onPress}}
        />
    </View>
  ); 
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 24
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
        fontWeight: 'bold',
        marginBottom: 12,
    },
    description: {
        fontSize: 16,
        textAlign: 'center',
        fontWeight: 'bold',
        lineHeight: 24,
        marginBottom: 24
    }
})

export default SubSlide;