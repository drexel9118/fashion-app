import * as React from 'react';
import { StyleSheet, View, Dimensions } from 'react-native';
import Slide from '../../components/Slide';

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import {interpolateColor, onScrollEvent, useValue} from  "react-native-redash/lib/module/v1";
import Animated, { multiply } from 'react-native-reanimated'
import SubSlide from '../../components/Subslide';
import { useRef } from 'react';


const {width, height} = Dimensions.get('window')
const slides = [
    {label: 'Relaxed', color: '#BFEAF5', title: 'Find Your Outfits', description: 'asd asdad asdas'},
    {label: 'Playful', color: '#BEECC4', title: 'Hear It First, Wear It first', description: 'asd asdad asdas'},
    {label: 'Excentric', color: '#FFE4D9', title: 'Your Style, Your Way', description: 'asd asdad asdas'},
    {label: 'Funny', color: '#FFDDDD', title: 'Looks Good, Feels Good', description: 'asd asdad asdas'}
]

const Onboarding: React.SFC = () => {
    const scroll = useRef<Animated.ScrollView>(null);
    const x = useValue(0)
    const onScroll = onScrollEvent({x})
    const backgroundColor = interpolateColor(x, {
        inputRange: slides.map((_, i)=> i * width),
        outputRange: slides.map((slide)=> slide.color)
    })

  return (
    <View style={styles.container}>
        <Animated.View style={[styles.slider, {backgroundColor}]}>
            <Animated.ScrollView
                ref={scroll} 
                horizontal  
                snapToInterval={width} 
                decelerationRate="fast"
                showsHorizontalScrollIndicator={false}
                bounces={false}
                scrollEventThrottle={1}
                {...{onScroll}}
                        > 
                {slides.map((slide, index)=> (
                    <Slide key={index} label={slide.label} right={!!(index % 2)} />
                ))}
            </Animated.ScrollView>
        </Animated.View>
        <View style={styles.footer}>
            <Animated.View style={{...StyleSheet.absoluteFillObject, backgroundColor}} />
            <Animated.View 
                style={
                    [styles.fotterContent, {width: width * slides.length, flex: 1, transform:[{translateX: multiply(x,-1)}]}]
                }
            >
                {slides.map(({title, description}, index)=> (
                        <SubSlide 
                            key={index} 
                            title={title} 
                            description={description} 
                            isLast={index === slides.length -1}
                            onPress={()=> {
                                if (scroll.current){
                                    // @ts-ignore
                                    scroll.current.scrollTo({ x: width * (index+1), animated: true });
                                }
                            }}
                            />
                    ))}
            </Animated.View>
        </View>
    </View>
  );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    slider: {
        height: 0.61 * height,
        backgroundColor: 'cyan',
        borderBottomRightRadius: 75
    },
    footer: {
        flex: 1
    },
    fotterContent: {
        flex: 1, 
        backgroundColor: 'white', 
        borderTopLeftRadius: 75,
        flexDirection: 'row',
    }
})

export default Onboarding;