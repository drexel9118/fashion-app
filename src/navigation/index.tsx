import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Onboarding from '../screens/onboarding/Onboarding';

const Stack = createStackNavigator();


const MainNavigation:React.SFC = ()=> {
    return (
        <NavigationContainer>
            <Stack.Navigator   
                screenOptions={{headerShown: false }}
            >
                <Stack.Screen name="Onboarding" component={Onboarding} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default MainNavigation