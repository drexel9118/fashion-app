import 'react-native-gesture-handler';
import * as React from 'react';
import MainNavigation from './navigation';

const App: React.SFC = () => {
  return (
    <MainNavigation/>
  );
}

export default App